package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.Goods;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @description 商品信息
 */
public interface GoodsDao {


    String getMaxCode();


    List<Goods> getGoodsInventoryList(@Param("offSet") Integer offSet, @Param("pageRow") Integer pageRow, @Param("codeOrName") String codeOrName, @Param("goodsTypeId") Integer goodsTypeId);

    Integer getGoodsInventoryCount(@Param("codeOrName") String codeOrName, @Param("goodsTypeId") Integer goodsTypeId);

    List<Goods> getGoodsList(@Param("offSet") int offSet,@Param("rows") Integer rows,@Param("goodsName") String goodsName,@Param("goodsTypeId") Integer goodsTypeId);

    Integer getGoodsCount(@Param("goodsName") String goodsName, @Param("goodsTypeId") Integer goodsTypeId);

    Integer saveGoods(Goods goods);

    Integer updateGoods(Goods goods);

    Goods findByGoodsId(Integer goodsId);

    Integer deleteGoods(Integer goodsId);

    List<Goods> getNoInventoryQuantityList(@Param("offSet") Integer offSet, @Param("rows") Integer rows, @Param("nameOrCode") String nameOrCode);

    Integer getNoInventoryQuantityCount(String nameOrCode);

    List<Goods> getHasInventoryQuantityList(@Param("offSet")Integer offSet, @Param("rows")Integer rows, @Param("nameOrCode")String nameOrCode);

    Integer getHasInventoryQuantityCount(String nameOrCode);
}
