package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.OverflowList;
import com.atguigu.jxc.entity.OverflowListGoods;

import java.util.List;

public interface OverflowListGoodsMapper {
    List<OverflowList> getOverflowlist(String sTime, String eTime);

    List<OverflowListGoods> getOverflowListById(Integer overflowListId);
}
