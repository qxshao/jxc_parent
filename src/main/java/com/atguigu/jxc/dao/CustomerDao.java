package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.Customer;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface CustomerDao {
    List<Customer> getCustomerReturnlist(@Param("offSet") Integer offSet, @Param("rows") Integer rows, @Param("customerName") String customerName);

    Integer getCustomerReturnCount(String customerName);

    Integer saveCustomer(Customer customer);


    Integer updateCustomer(Customer customer);

    Integer deleteCustomer(Integer customerId);
}
