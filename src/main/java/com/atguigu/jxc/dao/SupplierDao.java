package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.Supplier;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface SupplierDao {
    List<Supplier> getSupplierList(@Param("offSet") Integer offSet, @Param("pageRow") Integer pageRow, @Param("supplierName") String supplierName);

    Object getSupplierCount(String supplierName);


    void updateSupplier(Supplier supplier);

    void saveSupplier(Supplier supplier);

    Integer deleteSupplier(String ids);
}
