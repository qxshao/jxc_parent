package com.atguigu.jxc.controller;


import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.Supplier;
import com.atguigu.jxc.service.SupplierService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
@RequestMapping("supplier")
public class SupplierController {


    @Autowired
    private SupplierService supplierService;
    @PostMapping("/list")
    public Map<String,Object> list(Integer page, Integer rows, String supplierName){
        return supplierService.list(page,rows,supplierName);
    }

    @PostMapping("/save")
    public ServiceVO save(Supplier supplier){
        return supplierService.save(supplier);
    }

    @RequestMapping("/delete")
    public ServiceVO delete(String ids){
        return supplierService.delete(ids);
    }
}
