package com.atguigu.jxc.controller;

import com.atguigu.jxc.service.DamageListGoodsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
@RequestMapping("/damageListGoods")
public class DamageListGoodsController {
    @Autowired
    private DamageListGoodsService damageListGoodsService;
    @RequestMapping("/list")
    public Map<String,Object> list(String sTime, String eTime){
        return damageListGoodsService.list(sTime,eTime);
    }

    @RequestMapping("/goodsList")
    public Map<String,Object> goodsList(Integer damageListId){
        return damageListGoodsService.goodsList(damageListId);
    }
}
