package com.atguigu.jxc.service;

import java.util.Map;

public interface OverflowListGoodsService {
    Map<String, Object> list(String sTime, String eTime);

    Map<String, Object> goodsList(Integer overflowListId);
}
