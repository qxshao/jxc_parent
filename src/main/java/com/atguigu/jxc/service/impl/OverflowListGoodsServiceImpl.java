package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.OverflowListGoodsMapper;
import com.atguigu.jxc.entity.OverflowList;
import com.atguigu.jxc.entity.OverflowListGoods;
import com.atguigu.jxc.service.OverflowListGoodsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
@Service
public class OverflowListGoodsServiceImpl implements OverflowListGoodsService {
    @Autowired
    private OverflowListGoodsMapper overflowListGoodsMapper;
    @Override
    public Map<String, Object> list(String sTime, String eTime) {
        HashMap<String, Object> map = new HashMap<>();
        List<OverflowList> overflowListList = overflowListGoodsMapper.getOverflowlist(sTime,eTime);
        map.put("rows", overflowListList);
        return map;
    }

    @Override
    public Map<String, Object> goodsList(Integer overflowListId) {

        HashMap<String, Object> map = new HashMap<>();
        List<OverflowListGoods> overflowListGoodsList = overflowListGoodsMapper.getOverflowListById(overflowListId);
        map.put("rows", overflowListGoodsList);
        return map;
    }
}
