package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.CustomerDao;
import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.Customer;
import com.atguigu.jxc.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Service
public class CustomerServiceImpl implements CustomerService {
    @Autowired
    private CustomerDao customerDao;
    @Override
    public Map<String, Object> list(Integer page, Integer rows, String customerName) {
        HashMap<String, Object> map = new HashMap<>();
        page = page == 0 ? 1 : page;
        int offSet = (page - 1) * rows;
        List<Customer> customerList = customerDao.getCustomerReturnlist(offSet,rows,customerName);
        map.put("rows", customerList);
        map.put("total", customerDao.getCustomerReturnCount(customerName));
        return map;
    }

    @Override
    public ServiceVO save(Customer customer) {
        Integer customerId = customer.getCustomerId();
        if (customerId==null){  //如果id为空
            //执行添加操作

            customerDao.saveCustomer(customer);
        }else {
            //执行修改操作
            customerDao.updateCustomer(customer);
        }
        return new ServiceVO(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS);
    }

    public ServiceVO delete(String ids) {

        String[] idArray = ids.split(",");
        for(String id : idArray){
            customerDao.deleteCustomer(Integer.parseInt(id));
        }
        return new ServiceVO<>(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS);
    }
}
