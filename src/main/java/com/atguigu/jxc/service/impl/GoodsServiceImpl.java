package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.GoodsDao;
import com.atguigu.jxc.domain.ErrorCode;
import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.Goods;
import com.atguigu.jxc.entity.Log;
import com.atguigu.jxc.service.CustomerReturnListGoodsService;
import com.atguigu.jxc.service.GoodsService;
import com.atguigu.jxc.service.LogService;
import com.atguigu.jxc.service.SaleListGoodsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @description
 */
@Service
public class GoodsServiceImpl implements GoodsService {

    @Autowired
    private GoodsDao goodsDao;

    @Override
    public ServiceVO getCode() {

        // 获取当前商品最大编码
        String code = goodsDao.getMaxCode();

        // 在现有编码上加1
        Integer intCode = Integer.parseInt(code) + 1;

        // 将编码重新格式化为4位数字符串形式
        String unitCode = intCode.toString();

        for(int i = 4;i > intCode.toString().length();i--){

            unitCode = "0"+unitCode;

        }
        return new ServiceVO<>(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS, unitCode);
    }

    @Override
    public Map<String, Object> listInventory(Integer page, Integer rows, String codeOrName, Integer goodsTypeId) {
        HashMap<String, Object> map = new HashMap<>();
        page = page == 0 ? 1 : page;
        int offSet = (page - 1) * rows;
        List<Goods> goodsList = goodsDao.getGoodsInventoryList(offSet,rows,codeOrName,goodsTypeId);

        map.put("rows", goodsList);
        map.put("total",goodsDao.getGoodsInventoryCount(codeOrName,goodsTypeId) );
        return map;
    }

    @Override
    public Map<String, Object> list(Integer page, Integer rows, String goodsName, Integer goodsTypeId) {
        HashMap<String, Object> map = new HashMap<>();
        page = page ==0 ? 1 : page;
        int offSet = (page-1) * rows;
        List<Goods> goodsList = goodsDao.getGoodsList(offSet,rows,goodsName,goodsTypeId);
        Integer tota = goodsDao.getGoodsCount(goodsName,goodsTypeId);
        map.put("rows",goodsList);
        map.put("total", tota);
        return map;
    }

    public ServiceVO save(Goods goods) {

        if(goods.getGoodsId() == null){
            goods.setLastPurchasingPrice(goods.getPurchasingPrice());
            goods.setInventoryQuantity(0);
            goods.setState(0);
            goodsDao.saveGoods(goods);
        }else{
            goodsDao.updateGoods(goods);
        }

        return new ServiceVO<>(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS);

    }

    @Override
    public ServiceVO delete(Integer goodsId) {

        Goods goods = goodsDao.findByGoodsId(goodsId);

        if (goods.getState() == 1) {
            return new ServiceVO<>(ErrorCode.STORED_ERROR_CODE, ErrorCode.STORED_ERROR_MESS);
        } else if (goods.getState() == 2) {
            return new ServiceVO<>(ErrorCode.HAS_FORM_ERROR_CODE, ErrorCode.HAS_FORM_ERROR_MESS);
        } else {
            goodsDao.deleteGoods(goodsId);
        }
        return new ServiceVO<>(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS);
    }

    @Override
    public Map<String, Object> getNoInventoryQuantity(Integer page, Integer rows, String nameOrCode) {
        HashMap<String, Object> map = new HashMap<>();
        page = page == 0 ? 1 :  page;
        int offSet = (page -1) * rows;
        List<Goods> goodsList = goodsDao.getNoInventoryQuantityList(offSet,rows,nameOrCode);
        map.put("rows", goodsList);
        map.put("total",goodsDao.getNoInventoryQuantityCount(nameOrCode));
        return map;
    }

    @Override
    public Map<String, Object> getHasInventoryQuantity(Integer page, Integer rows, String nameOrCode) {
        HashMap<String, Object> map = new HashMap<>();
        page = page == 0 ? 1 : page;
        int offSet = (page - 1) * rows;
        List<Goods> goodsList = goodsDao.getHasInventoryQuantityList(offSet,rows,nameOrCode);
        map.put("rows", goodsList);
        map.put("total", goodsDao.getHasInventoryQuantityCount(nameOrCode));
        return map;
    }

    @Override
    public ServiceVO saveStock(Integer goodsId, Integer inventoryQuantity, double purchasingPrice) {
        Goods goods = goodsDao.findByGoodsId(goodsId);
        goods.setInventoryQuantity(inventoryQuantity);
        goods.setPurchasingPrice(purchasingPrice);
        goodsDao.updateGoods(goods);
        return new ServiceVO(SuccessCode.SUCCESS_CODE,SuccessCode.SUCCESS_MESS);
    }

    @Override
    public ServiceVO deleteStock(Integer goodsId) {
        Goods goods = goodsDao.findByGoodsId(goodsId);
        Integer state = goods.getState();
        if (state == 2){
            return new ServiceVO(ErrorCode.HAS_FORM_ERROR_CODE,ErrorCode.HAS_FORM_ERROR_MESS);
        }
        goods.setInventoryQuantity(0);
        goodsDao.updateGoods(goods);
        return new ServiceVO(SuccessCode.SUCCESS_CODE,SuccessCode.SUCCESS_MESS);
    }


}
