package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.DamageListGoodsDao;
import com.atguigu.jxc.entity.DamageListGoods;
import com.atguigu.jxc.service.DamageListGoodsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class DamageListGoodsServiceImpl implements DamageListGoodsService {

    @Autowired
    private DamageListGoodsDao damageListGoodsDao;
    @Override
    public Map<String, Object> list(String sTime, String eTime) {
        HashMap<String, Object> map = new HashMap<>();
        List <DamageListGoods> damageListGoodsList = damageListGoodsDao.getDamageList(sTime,eTime);
        map.put("rows", damageListGoodsList);
        return map;
    }

    @Override
    public Map<String, Object> goodsList(Integer damageListId) {
        HashMap<String, Object> map = new HashMap<>();
        List<DamageListGoods> damageList = damageListGoodsDao.goodsList(damageListId);
        map.put("rows", damageList);
        return map;
    }

}
